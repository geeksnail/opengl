package com.example.opengldemo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.ArrayList;

import javax.microedition.khronos.opengles.GL10;

import android.content.res.AssetManager;
import android.opengl.GLU;
import android.opengl.Matrix;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.ScaleGestureDetector.OnScaleGestureListener;

public class DrawCube extends OpenGLESActivity implements IOpenGLDemo {

	float vArrayAxis[] = { -0.8f, 0.0f, 0.8f, 0.0f, 0.0f, -0.8f, 0.0f, 0.8f };
	int NUM = 12;
	float[] vArrayCube;
	Object[] arrayInFile;
	int index = 0;
	FloatBuffer vertexAxis, vertexCube;

	// Parameters for transformation
	float mScaleFactor = 1;

	float mTranslateX = 0;
	float mTranslateY = 0;
	float mTranslateZ = 0;

	// The movement of secondary finger
	float mTranslateX1 = 0;
	float mTranslateY1 = 0;
	float mTranslateZ1 = 0;

	// The angle in radiant
	double mAngle;
	float mRotateX;
	float mRotateY;
	float mRotateZ = 1.0f;

	boolean mNeedScale;
	boolean mNeedTranslate;
	boolean mNeedRoate;

	ScaleGestureDetector scaleGestureDetector;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		AssetManager am = this.getAssets();
		scaleGestureDetector = new ScaleGestureDetector(this,
				new OnScaleGestureListener() {

					@Override
					public boolean onScale(ScaleGestureDetector detector) {
						mScaleFactor *= detector.getScaleFactor();
						mNeedScale = true;
						return true;
					}

					@Override
					public boolean onScaleBegin(ScaleGestureDetector detector) {
						// TODO Auto-generated method stub
						return true;
					}

					@Override
					public void onScaleEnd(ScaleGestureDetector detector) {
						// TODO Auto-generated method stub

					}
				});
		try {
			InputStream is = am.open("dino.dat");
			arrayInFile = populateArrays(is);
			vArrayCube = new float[arrayInFile.length];
			for (int i = 0; i < arrayInFile.length; i++) {
				vArrayCube[i] = Float.parseFloat((String) arrayInFile[i]);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		ByteBuffer vbb2 = ByteBuffer.allocateDirect(vArrayCube.length * 4);
		vbb2.order(ByteOrder.nativeOrder());
		vertexCube = vbb2.asFloatBuffer();
		vertexCube.put(vArrayCube);
		vertexCube.position(0);

		ByteBuffer vbb = ByteBuffer.allocateDirect(vArrayAxis.length * 4);
		vbb.order(ByteOrder.nativeOrder());
		vertexAxis = vbb.asFloatBuffer();
		vertexAxis.put(vArrayAxis);
		vertexAxis.position(0);
	}

	private FloatBuffer getOpenGlBuffer(float[] buffer) {
		ByteBuffer vbb = ByteBuffer.allocateDirect(buffer.length * 4);
		vbb.order(ByteOrder.nativeOrder());
		FloatBuffer floatBuffer = vbb.asFloatBuffer();
		floatBuffer.put(buffer);
		floatBuffer.position(0);
		return floatBuffer;
	}

	public Object[] populateArrays(InputStream is) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		String aa;
		ArrayList<String> array = new ArrayList<String>();
		while ((aa = br.readLine()) != null) {
			String[] index = aa.split(" ");
			for (int i = 0; i < index.length; i++) {
				array.add(index[i]);
			}
		}
		return array.toArray();
	}

	public void drawPolyLine(GL10 gl) {
		gl.glVertexPointer(3, GL10.GL_FLOAT, 0, vertexCube);
		gl.glColor4f(0.0f, 1.0f, 0.0f, 1.0f);
		gl.glLineWidth(4);
		gl.glDrawArrays(GL10.GL_LINE_STRIP, 0, 16);
	}

	public void DrawAxis(GL10 gl) {
		// Tell OpenGl where we have stored our vertices
		gl.glVertexPointer(2, GL10.GL_FLOAT, 0, vertexAxis);

		gl.glColor4f(1.0f, 0.0f, 0.0f, 1.0f);
		gl.glLineWidth(8);
		gl.glDrawArrays(GL10.GL_LINES, 0, 4);
	}

	private float[] mTranslateMatrix = { 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0,
			0, 0, 1 };
	private float[] mScaleMatrix = { 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0,
			0, 1 };
	private float[] mRotateMatrix = { 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0,
			0, 1 };

	FloatBuffer mTranslateMatrixBuffer, mScaleMatrixBuffer,
			mRotateMatrixBuffer;

	// Normalize the matrix
	private void normalize(float[] matrix) {
		for (int i = 0; i < matrix.length; i++) {
			if (i == 0 || i == 5 || i == 10 || i == 15) {
				matrix[i] = 1;
			} else {
				matrix[i] = 0;
			}
		}
	}

	public void DrawScene(GL10 gl) {
		super.DrawScene(gl);

		gl.glMatrixMode(GL10.GL_MODELVIEW);
		gl.glLoadIdentity();
		gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
		DrawAxis(gl);
		// //////////////////////////////////////////Scale/////////////////////////////////////////////
		mScaleMatrix[0] = mScaleFactor;
		mScaleMatrix[5] = mScaleFactor;
		mScaleMatrix[10] = mScaleFactor;

		mScaleMatrixBuffer = getOpenGlBuffer(mScaleMatrix);
		gl.glMultMatrixf(mScaleMatrixBuffer);

		// //////////////////////////////////////////Translate/////////////////////////////////////////
		
		float length = (float) Math.sqrt(mTranslateX * mTranslateX
				+ mTranslateY * mTranslateY);
		if (length != 0) {
			if(hasTwoFinger == false) {
				mTranslateMatrix[3] = mTranslateX / length;
				mTranslateMatrix[7] = -mTranslateY / length;
			}
			mTranslateMatrix[11] = -0.6f;

			mTranslateMatrixBuffer = getOpenGlBuffer(mTranslateMatrix);
			gl.glMultMatrixf(mTranslateMatrixBuffer);
		}
		
		// ///////////////////////////////////////////////////////Rotate//////////////////////////////////
		if (mRotateZ == 1.0f) {
			normalize(mRotateMatrix);
			mRotateMatrix[0] = (float) Math.cos(-mAngle);
			mRotateMatrix[1] = (float) -Math.sin(-mAngle);
			mRotateMatrix[4] = (float) Math.sin(-mAngle);
			mRotateMatrix[5] = (float) Math.cos(-mAngle);
			mRotateMatrixBuffer = getOpenGlBuffer(mRotateMatrix);
			gl.glMultMatrixf(mRotateMatrixBuffer);
		} else if (mRotateX == 1.0f) {
			normalize(mRotateMatrix);
			mRotateMatrix[5] = (float) Math.cos(-mAngle);
			mRotateMatrix[6] = (float) -Math.sin(-mAngle);
			mRotateMatrix[9] = (float) Math.sin(-mAngle);
			mRotateMatrix[10] = (float) Math.cos(-mAngle);
			mRotateMatrixBuffer = getOpenGlBuffer(mRotateMatrix);
			gl.glMultMatrixf(mRotateMatrixBuffer);
		} else if (mRotateY == 1.0f) {
			normalize(mRotateMatrix);
			mRotateMatrix[0] = (float) Math.cos(-mAngle);
			mRotateMatrix[2] = (float) -Math.sin(-mAngle);
			mRotateMatrix[8] = (float) Math.sin(-mAngle);
			mRotateMatrix[10] = (float) Math.cos(-mAngle);
			mRotateMatrixBuffer = getOpenGlBuffer(mRotateMatrix);
			gl.glMultMatrixf(mRotateMatrixBuffer);
		}

		drawPolyLine(gl);

		gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
	}

	private float mLastX;
	private float mLastY;

	// For secondary pointer.
	private float mLastX1;
	private float mLastY1;

	private float dx;
	private float dy;

	boolean rotateMode = false;
	boolean twoFingerUpMode = true;
	boolean twoFingerDownMode = false;
	private String TAG = "DrawCube";
	boolean hasTwoFinger = false;

	@Override
	public boolean onTouchEvent(MotionEvent e) {
		scaleGestureDetector.onTouchEvent(e);
		switch (e.getAction() & MotionEvent.ACTION_MASK) {
		case MotionEvent.ACTION_POINTER_DOWN:
			dx = e.getX() - e.getX(1);
			dy = e.getY() - e.getY(1);

			mLastX1 = e.getX(1);
			mLastY1 = e.getY(1);
			hasTwoFinger = true;
			break;
		case MotionEvent.ACTION_DOWN:
			mLastX = e.getX();
			mLastY = e.getY();
			break;
		case MotionEvent.ACTION_MOVE:
			mNeedTranslate = true;
			mTranslateX += e.getX() - mLastX;
			mTranslateY += e.getY() - mLastY;

			mLastX = e.getX();
			mLastY = e.getY();

			if (hasTwoFinger) {
				mTranslateX1 += e.getX(1) - mLastX1;
				mTranslateY1 += e.getY(1) - mLastY1;

				mLastX1 = e.getX(1);
				mLastY1 = e.getY(1);

				double angle = 180 / 3.14 * getAngleBetweenTwoVector(
						mTranslateX, mTranslateY, mTranslateX1, mTranslateY1);
				angle = Math.abs(angle);
				Log.d(TAG, "angle between two move: " + angle);
				// Two finger move up
				if (angle < 30 && mTranslateY > 0 && mTranslateY1 > 0) {
					twoFingerUpMode = true;
				} else if (angle < 30 && mTranslateY < 0 && mTranslateY1 < 0) {
					twoFingerDownMode = true;
				} else if (angle > 90) {
					rotateMode = true;
				}

				if (rotateMode) {
					mRotateX = 0f;
					mRotateY = 0;
					mRotateZ = 1.0f;

					mAngle += getAngleBetweenTwoVector(dx, dy,
							e.getX() - e.getX(1), e.getY() - e.getY(1));

					dx = e.getX() - e.getX(1);
					dy = e.getY() - e.getY(1);
				}

				if (twoFingerUpMode) {
					mRotateX = 1.0f;
					mRotateY = 0;
					mRotateZ = 0;

					mAngle += 0.1f;
				}

				if (twoFingerDownMode) {
					mRotateX = 0;
					mRotateY = 1.0f;
					mRotateZ = 0;

					mAngle += 0.1f;
				}
			}
			break;
		case MotionEvent.ACTION_UP:
			hasTwoFinger = false;
			break;
		case MotionEvent.ACTION_POINTER_UP:
			hasTwoFinger = false;
			break;
		}
		return true;
	}

	/**
	 * Get the radiant of the two vectors. If the second vector is right of the
	 * first one, the result is negative otherwise positive. For the rotate
	 * method rotate the object in counter-clock direction.
	 * 
	 * @param x1
	 *            The x coordinate of the first vector
	 * @param y1
	 *            The y coordinate of the first vector
	 * @param x2
	 *            The x coordinate of the second vector
	 * @param y2
	 *            The y coordinate of the second vector
	 * @return The radiant between the two vectors
	 */
	double getAngleBetweenTwoVector(float x1, float y1, float x2, float y2) {

		float length1 = (float) Math.sqrt(x1 * x1 + y1 * y1);
		float length2 = (float) Math.sqrt(x2 * x2 + y2 * y2);
		float dot_mult = (x1 / length1) * (x2 / length2) + (y1 / length1)
				* (y2 / length2);

		if (isCounterClockRotate(x1, y1, x2, y2)) {
			return -Math.acos((double) dot_mult);
		} else {
			return Math.acos((double) dot_mult);
		}
	}

	/**
	 * Check if the rotate is counter-clock direction Assuming every move is
	 * very small step, so we can ignore the situation that move from one
	 * quadrant to another we assume every move is in one quadrant.
	 * 
	 * @return
	 */
	private boolean isCounterClockRotate(float x1, float y1, float x2, float y2) {
		if ((y2 > 0 && x2 < x1) || (y2 < 0 && x2 > x1)) {
			return true;
		}
		return false;
	}
}
